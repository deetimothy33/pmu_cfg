##
# the purpose of this file is to compare
# different machine learning classifiers
# and determine which is the best for solving
# this problem
##
library(caret)

source("data_read.r")

##
# constants
##
FOLDER_NAME <- "../code/output"
FILE_NAME <- "context_print.csv"

##
# begin script
##
data <- read_raw_data(paste0(FOLDER_NAME, "/", FILE_NAME))

# format te raw data
#data <- format_data(raw_data)

# print(head(data))
# stopifnot(F)

# remove NA from the data
before_removal <- nrow(data)
data <- na.omit(data)
print(paste("NA rows removed:", before_removal - nrow(data)))

# define the machine learning methods to use
method_list_test <- c("svmRadial", "svmLinear2")
method_list_good <- c("svmLinear2", "svmRadial", "svmPoly", "ranger", "nb")
method_list_alternate <- c("xyf", "rpart", "pam", "nnet", "gamSpline", "bayesglm", "svmRadialWeights", "evtree")
method_list <- method_list_test #TODO change to _good

# make a list for models
model_list <- vector("list", length(method_list))
names(model_list) <- method_list

# prepare training scheme(S)
# 3 repeats of 10 fold crossvalidation
control <- trainControl(method="repeatedcv", number=10, repeats=3, timingSamps=20)

# extract featues and classification
X = data[c(-1)]
y = factor(data$basic_block_executed)

# provide furthere restrictions on X to test 1 variable at a time
# programmable counters
#X = X[c(1:4)]
#X = X[c(5:8)]
#X = X[c(9:12)]
#X = X[c(13:16)]

# fixed counters
#X = X[c(17:20)]
#X = X[c(21:24)]
#X = X[c(25:28)]

# single fixed counters
X = X[c(17,21,25)]

# single programmable counters and fixed counters
#X = X[c(17,21,25,1,5,9,13)]

# all (best)
#X = X[c(1:20)]
#TODO

#print(head(data))
print(head(X))
#print(head(y))
#stopifnot(FALSE)

# train each model in model list
for(i in 1:length(method_list)){
	print(paste("begin", method_list[i]))

	# tuneLength is the number of parameter values to try for each model
	# tain(X, y, method, control, tuneLength)
	#
	#model_list[[i]] <- train(basic_block_executed~., data=data,
	#    method=method_list[i], trControl=control, tuneLength=3)
	#model_list[[i]] <- train(data, factor(data$basic_block_executed),
	#	method=method_list[i], trControl=control, tuneLength=3)
	
	model_list[[i]] <- train(X, y, method=method_list[i], trControl=control, tuneLength=3)
}

# print the results for each model
lapply(model_list, print)

stopifnot(F)


##
# comparing multiple models
##

# use carret resample to find the best of the best models
# collect resamples given list of models
results <- resamples(model_list)

# summarize the results
summary(results)

#
# test that the differences in models are significant
#
# difference in model predictions
diffs <- diff(results)
# summarize p-values for pair-wise comparisons
summary(diffs)

###
# PLOTS
###
pdf("output/classifier_parallel_plot.pdf")
parallelplot(results)
dev.off()

pdf("output/classifier_splom_plot.pdf")
splom(results)
dev.off()

pdf("output/classifier_density_plot.pdf")
densityplot(results)
dev.off()

# boxplots of results
pdf("output/classifier_box_plot.pdf")
bwplot(results)
dev.off()

# dot plots of results
pdf("output/classifier_dot_plot.pdf")
dotplot(results)
dev.off()

# ALSO: print all plots in one file for easy viewing
pdf("output/classifier_all.pdf")
#xyplot(results)
parallelplot(results)
splom(results)
densityplot(results)
bwplot(results)
dotplot(results)
dev.off()
