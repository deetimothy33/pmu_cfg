#
# goal: read in a data
#

##
# read raw data from folder
# one data set per file
#
# each row in the matrix returned is one file
##
read_raw_data <- function(file_name){
	data <- read.csv(file_name, header=TRUE)

	# exclude columns 1,2
	data <- data[c(-1,-2)]

	return(data)
}

##
# format the data
# such that it is dimensioned
# (x = classification, y = feature list, z = feature values).
#
# input matrix is ([file size], 4)
# where column [,4] is the classification of the data
#
# output is a data.frame() where each row is 1 file
# each column contains a list with one feature
##
format_data <- function(data){
	class_list <- c()
	for(i in 1:nrow(data)){
		class_list <- c(class_list, data[i,ncol(data)][[1]][[1]])
	}

	data <- data.frame(
			   "classification" = class_list,
			   "response" = I(data[,c(1:(ncol(data)-1))]))

	return(data)
}

##
# test functions
##

#raw_data <- read_raw_data("../code/output/context_print.csv")
#print(raw_data)
#print(class(raw_data))

#data <- format_data(raw_data)
#print(data)

