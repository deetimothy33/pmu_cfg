#include <stdio.h> 
#include <udis86.h>

using namespace std;

int main(){
	// disassemble the instruction
	ud_t ud_obj;
	unsigned char inst_buff[8];

	ud_init(&ud_obj);
	ud_set_input_buffer(&ud_obj, inst_buff, 8);
	ud_set_mode(&ud_obj, 64);
	ud_set_syntax(&ud_obj, UD_SYN_INTEL);

	// populate instruction buffer
	unsigned long long inst = 0xAFAFAFAFAFAFAFAF;
	for(int i=0; i<8; i++){
		// shift by 8*i bits and take the lsb in buffer
		//TODO perhaps the inst is the other endian?
		inst_buff[i] = (inst >> (8*i)) & 0x00000000000000FF;
	}
	//TODO
	// split the disassembly into a separate application compiled with gcc

	// ud_disassemble fills the ud_obj struct with data
	if (ud_disassemble(&ud_obj) == 0) printf("error");

	// output instruction to file
	printf("%s", ud_insn_asm(&ud_obj));

	return 0;
}
