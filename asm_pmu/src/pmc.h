#ifndef PMC_H
#define PMC_H

#include <unistd.h>
#include <iostream>
#include <string>
#include <vector>

#include "MSR.h"

using namespace std;

#define rdpmc(counter,low,high) \
     __asm__ __volatile__("rdpmc" \
        : "=a" (low), "=d" (high) \
        : "c" (counter))

// enumerate the possible counter values
enum counter_ecx{
	PMC_0 = 0,
	PMC_1,
	PMC_2,
	PMC_3,
	FC_0 = 0x40000000,
	FC_1,
	FC_2
};

typedef struct{
	string name;
	volatile unsigned addr;
	unsigned long long data;
} pmc_t;

#define N_PMC 7
pmc_t pmc[N_PMC] = {
	{"pmc_0",PMC_0,0},
	{"pmc_1",PMC_1,0},
	{"pmc_2",PMC_2,0},
	{"pmc_3",PMC_3,0},
	{"inst_retired_any",FC_0,0},
	{"cpu_clk_unhaulted_thread",FC_1,0},
	{"cpu_clk_unhaulted_ref",FC_2,0}
};

MSR::programmable_counter_event pce[] = {
	MSR::MEM_LOAD_RETIRED_L3_MISS,
	//MSR::MEM_LOAD_RETIRED_L3_UNSHARED_HIT,
	//MSR::MEM_LOAD_RETIRED_L2_HITM,
	//MSR::MEM_LOAD_RETIRED_L2_HIT,
	//MSR::MEM_LOAD_UOPS_MISC_RETIRED_LLC_MISS,
	MSR::MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_NONE,
	//MSR::MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_HITM,
	//MSR::MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP, 
	MSR::MEM_LOAD_UOPS_RETIRED_L2_HIT,
	//MSR::SKL_MEM_LOAD_RETIRED_L3_MISS,
	MSR::SKL_MEM_LOAD_RETIRED_L3_HIT,
	//MSR::SKL_MEM_LOAD_RETIRED_L2_MISS,
	//MSR::SKL_MEM_LOAD_RETIRED_L2_HIT,
	//MSR::ARCH_LLC_REFERENCE,    
	//MSR::ARCH_LLC_MISS,     
	//MSR::ATOM_MEM_LOAD_RETIRED_L2_HIT,
	//MSR::ATOM_MEM_LOAD_RETIRED_L2_MISS	
};

// used in setup and teardown process
std::vector<MSR*> msr_vector;

// setup all pmc
void setup_pmc(){
	// create MSR list
	msr_vector = MSR::create_MSR_vector();

	// ask each MSR to perform its setup routine
	for(vector<MSR*>::iterator it = msr_vector.begin(); it != msr_vector.end(); ++it) (*it)->setup(pce);
	
	// enable pmu
	for(vector<MSR*>::iterator it = msr_vector.begin(); it != msr_vector.end(); ++it) (*it)->enable();
}

void teardown_pmc(){
	for(vector<MSR*>::iterator it = msr_vector.begin(); it != msr_vector.end(); ++it) (*it)->disable();
}

// read all pmc
pmc_t* read_pmc(){
	volatile unsigned a,d;

	for(int i=0; i<N_PMC; i++){
		rdpmc(pmc[i].addr,a,d);
		pmc[i].data = ((unsigned long)a) | (((unsigned long)d) << 32);
	}

	return pmc;
}

// print current pmc values last read with read_pmc()
void print_pmc(){
	for(int i=0; i<N_PMC; i++){
		cout << pmc[i].name << ": " << pmc[i].data << endl; 
	}	
}

#endif

