#include <iostream>
#include <fstream>
#include <string>

#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <inttypes.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>

#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/reg.h>

//#include <udis86.h>
#include "pmc.h"
#include "util.h"

using namespace std;

#define OUTPUT_FILE "out/asm.csv"

/**
 * print out every assembly instruction
 */
void monitor(int pid){
	int counter = 0;
	int wait_val;
	unsigned long long inst;
	user_regs_struct regs;

	// setup pmc for reading
	//setup_pmc();
	//TODO

	// open output file for writing
	ofstream file(OUTPUT_FILE, ofstream::trunc);
	file << "instruction" << endl;

	wait(&wait_val);
	// the child is finished; wait_val != 1407
	while (WIFSTOPPED(wait_val)) {
		// increase instruction counter
		counter++;

		// read the current instruction
		ptrace(PTRACE_GETREGS, pid, NULL, &regs);
		inst = ptrace(PTRACE_PEEKTEXT, pid, regs.rip, NULL);
		if(inst == 0xFFFFFFFFFFFFFFFF && errno) cout << "instruction not read" << endl;

		// find some way of disambeling this!!!!!!!!
		//TODO
		
		file << inst << endl;

		/* Make the child execute another instruction */
		if (ptrace(PTRACE_SINGLESTEP, pid, 0, 0) != 0) {
			perror("ptrace");
		}

		wait(&wait_val);
	}

	// un-setup pmc
	//teardown_pmc();
	//TODO

	file.close();

	printf("instructions executed: %d\n", counter);
}

int main(){
	cout << "monitor program and print out asm" << endl;

	// start program to monitor 
	// (gzip will only do compression with the -f option)
	char program[] = "/bin/gzip";
	char* args[] = {"-f", "data.txt", NULL};
	//char* args[] = {"data.txt", NULL};
	
	int pid = start_program(program, args);
	monitor(pid);
	
	cout << "monitoring complete" << endl;
	
	int status;
	while(waitpid(pid, &status, WNOHANG) == 0){
		sleep(1);
	}

	cout << "child exited" << endl;
	cout << "unzipping" << endl;

	char* args0[] = {"-f", "data.txt.gz", NULL};	
	gunzip(args0);
	while(waitpid(pid, &status, WNOHANG) == 0) sleep(1);
	
	cout << "unzipping complete" << endl;	
	return status;
}
