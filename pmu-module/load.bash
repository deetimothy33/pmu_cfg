#! /bin/bash

# load and info
modinfo pmu-device2.ko
insmod pmu-device2.ko

# make sure the module was inserted properly
cat /proc/modules | grep pmu_device2
cat /proc/devices | grep PMU_DEVICE2_SWAMY

# create character device in .
mknod PMU_DEVICE2_SWAMY c 100 0
chmod a+w PMU_DEVICE2_SWAMY
