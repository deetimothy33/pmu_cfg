#! /bin/bash

# perform system setup
sudo ./disable_nmi_watchdog.bash
sudo ./enable_rdpmc.bash
sudo modprobe msr

# load, test, unload module
sudo ./load.bash
sudo nice -n -20 ./pmu.x
sudo ./unload.bash
