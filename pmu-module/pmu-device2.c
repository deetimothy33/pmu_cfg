//simple driver for PMU
//author - swamy d ponpandi
//date - June 8, 2017
//
//notes - adapated from Linux kernel module programming
//
//modifications - added a new function to detect CPU using cpuid instruction
//
//modifications - changed ioctl to unlocked_ioctl
//                unregister_chrdev is void return type now, changed it.
//                device_ioctl does not have inode as param anymore, changed it
//                device_ioctl is return type is now long

/*
 *  chardev.c - Create an input/output character device
 */

#include <linux/kernel.h>	/* We're doing kernel work */
#include <linux/module.h>	/* Specifically, a module */
#include <linux/fs.h>
#include <asm/uaccess.h>	/* for get_user and put_user */
#include <asm/perf_event.h>   //added by swamy

// used for getting current instruction
#include <asm/current.h>
#include <linux/sched.h>
#include <asm/processor.h>

// used to disable premption while monitored program is executing
#include <linux/preempt.h>
#include <linux/sched.h>
#include <linux/smp.h>

// for allocating memory
#include <linux/slab.h>

//our device driver modelled after existing char drivers --swamy
#include "pmu-device2.h"
#define SUCCESS 0
#define DEVICE_NAME "PMU_DEVICE2_SWAMY"
#define BUF_LEN 80

// added by tim to organize perfmon code inclusion
#include "perfmon_code.h"

// provides inline asm functions to read pmu_values
//#include "pmc.h"
#include "pmc.c"
#include "types.h"

// number of measurement blocks to allocate at one time
// (didn't help incrase the granularity)
#define KALLOC_BLOCK_N 1 

/**
 * used to make a linked list in order to store
 * counter values in the driver
 */
typedef struct pmc_node_t pmc_node_t;
struct pmc_node_t{
	pmc_value_t pmc;
	pmc_node_t *next;
};

/** 
 * head of the buffer storing pmc values
 * when reading from the device, head gets advanced along the list
 * and nodes get recycled to the end of the list
 * *head is the block before the next block to be read by read*
 * therefore 
 * if current == head
 * then all measurements have been read
 *
 * current is the most resently written node
 *
 * tail is the last node in the list
 * the ->next of tail is always 0
 */
static pmc_node_t* pmc_node_head;
static pmc_node_t* pmc_node_current;
static pmc_node_t* pmc_node_tail;

/* 
 * Is the device open right now? Used to prevent
 * concurent access into the same device 
 */
static int Device_Open = 0;

/* 
 * The message the device will give when asked 
 */
static char Message[BUF_LEN];

/* 
 * How far did the process reading the message get?
 * Useful if the message is larger than the size of the
 * buffer we get to fill in device_read. 
 */
static char *Message_Ptr;

//cpuid function code taken from Perfmon --swamy, TODO : add license 
/*
 * .byte 0x53 == push ebx. it's universal for 32 and 64 bit
 * .byte 0x5b == pop ebx.
 * Some gcc's (4.1.2 on Core2) object to pairing push/pop and ebx in 64 bit mode.
 * Using the opcode directly avoids this problem.
 */
	static inline void
cpuid_pmu(unsigned int op, unsigned int *a, unsigned int *b, unsigned int *c, unsigned int *d)
{
	__asm__ __volatile__ (".byte 0x53\n\tcpuid\n\tmovl %%ebx, %%esi\n\t.byte 0x5b"
			: "=a" (*a),
			"=S" (*b),
			"=c" (*c),
			"=d" (*d)
			: "a" (op));
}

/* 
 * This is called whenever a process attempts to open the device file 
 */
static int device_open(struct inode *inode, struct file *file)
{
#ifdef DEBUG
	printk(KERN_INFO "device_open(%p)\n", file);
#endif

	/* 
	 * We don't want to talk to two processes at the same time 
	 */
	if (Device_Open)
		return -EBUSY;

	Device_Open++;
	/*
	 * Initialize the message 
	 */
	Message_Ptr = Message;
	try_module_get(THIS_MODULE);

	/**
	 * initialize the structure to store pmc_values
	 */
	pmc_node_head = kmalloc(sizeof(pmc_node_t), GFP_KERNEL);
	pmc_node_current = pmc_node_head;
	pmc_node_tail = pmc_node_head;

	pmc_node_head->next = 0;

	return SUCCESS;
}

static int device_release(struct inode *inode, struct file *file)
{
#ifdef DEBUG
	printk(KERN_INFO "device_release(%p,%p)\n", inode, file);
#endif

	/* 
	 * We're now ready for our next caller 
	 */
	Device_Open--;

	module_put(THIS_MODULE);

	/**
	 * free any remaining pmc_values which have not been read
	 */
	pmc_node_t* temp;
	while(pmc_node_head != 0){
		temp = pmc_node_head;
		pmc_node_head = pmc_node_head->next;

		kfree(temp);
	}

	return SUCCESS;
}

/* 
 * This function is called whenever a process which has already opened the
 * device file attempts to read from it.
 */
static ssize_t device_read(struct file *file,	/* see include/linux/fs.h   */
		char __user * buffer,	/* buffer to be
					 * filled with data */
		size_t length,	/* length of the buffer     */
		loff_t * offset)
{
	// disable premption while reading counters
	preempt_disable();
	
	/* 
	 * Number of bytes actually written to the buffer 
	 */
	int bytes_read = 0;
	
	// get pmu counter values
	//pmu_read_all_counters(&pmc);
	
	// return one reading of the pmc (the oldest one)
	if(pmc_node_head != pmc_node_current){
		char *pmc_c = (char*)&(pmc_node_head->next->pmc);

		while(length){
			put_user(*(pmc_c++), buffer++);
			length--;
			bytes_read++;
		}

		// move the head of the list forward one block
		// move the block that was just read to th eend of the list
		pmc_node_tail->next = pmc_node_head;
		pmc_node_tail = pmc_node_head;

		pmc_node_head = pmc_node_head->next;
		pmc_node_tail->next = 0;
	}else{
		// there is nothing to read
		// write all 0's into the buffer
		while(length){
			put_user(0, buffer++);
			length--;
			bytes_read++;
		}
	}

#ifdef DEBUG
	printk(KERN_INFO "device_read(%p,%p,%d)\n", file, buffer, length);
#endif

	/* 
	 * If we're at the end of the message, return 0
	 * (which signifies end of file) 
	 */
	//if (*Message_Ptr == 0) return 0;

	/* 
	 * Actually put the data into the buffer 
	 *
	while (length && *Message_Ptr) {

		* 
		 * Because the buffer is in the user data segment,
		 * not the kernel data segment, assignment wouldn't
		 * work. Instead, we have to use put_user which
		 * copies data from the kernel data segment to the
		 * user data segment. 
		 *
		put_user(*(Message_Ptr++), buffer++);
		length--;
		bytes_read++;
	}
	*/

#ifdef DEBUG
	printk(KERN_INFO "Read %d bytes, %d left\n", bytes_read, length);
#endif

	/* 
	 * Read functions are supposed to return the number
	 * of bytes actually inserted into the buffer 
	 */
	preempt_enable();
	return bytes_read;
}

/* 
 * This function is called when somebody tries to
 * write into our device file. 
 */
	static ssize_t
device_write(struct file *file,
		const char __user * buffer, size_t length, loff_t * offset)
{
	int i;

#ifdef DEBUG
	printk(KERN_INFO "device_write(%p,%s,%d)", file, buffer, length);
#endif

	for (i = 0; i < length && i < BUF_LEN; i++)
		get_user(Message[i], buffer + i);

	Message_Ptr = Message;

	/* 
	 * Again, return the number of input characters used 
	 */
	return i;
}

/* 
 * This function is called whenever a process tries to do an ioctl on our
 * device file. We get two extra parameters (additional to the inode and file
 * structures, which all device functions get): the number of the ioctl called
 * and the parameter given to the ioctl function.
 *
 * If the ioctl is write or read/write (meaning output is returned to the
 * calling process), the ioctl call returns the output of this function.
 *
 */
//int device_ioctl(struct inode *inode,	/* see include/linux/fs.h */
//		 struct file *file,	/* ditto */
//		 unsigned int ioctl_num,	/* number and param for ioctl */
//		 unsigned long ioctl_param)*/
long device_ioctl(/* see include/linux/fs.h */
		struct file *file,	
		unsigned int ioctl_num,	/* number and param for ioctl */
		unsigned long ioctl_param)
{
	int i;
	char *temp;
	char ch;
	unsigned int a, b, c, d;
	char buffer[64];

	// added by tim to store pmu counter values       
	pmc_value_t pmc, pmc_test, *pmc_temp;

	struct pt_regs *regs;
	unsigned long long inst, v1, v2;
			
	struct task_struct *task;
	int cpu_id;

	//	struct x86_pmu_capability pmuinfo;
	/* 
	 * Switch according to the ioctl called 
	 */
	switch (ioctl_num) {
		case IOCTL_SET_MSG:
			/* 
			 * Receive a pointer to a message (in user space) and set that
			 * to be the device's message.  Get the parameter given to 
			 * ioctl by the process. 
			 */
			temp = (char *)ioctl_param;

			/* 
			 * Find the length of the message 
			 */
			get_user(ch, temp);
			for (i = 0; ch && i < BUF_LEN; i++, temp++)
				get_user(ch, temp);

			device_write(file, (char *)ioctl_param, i, 0);
			break;

		case IOCTL_GET_MSG:
			/* 
			 * Give the current message to the calling process - 
			 * the parameter we got is a pointer, fill it. 
			 */
			i = device_read(file, (char *)ioctl_param, 99, 0);

			/* 
			 * Put a zero at the end of the buffer, so it will be 
			 * properly terminated 
			 */
			put_user('\0', (char *)ioctl_param + i);
			break;

		case IOCTL_GET_NTH_BYTE:
			/* 
			 * This ioctl is both input (ioctl_param) and 
			 * output (the return value of this function) 
			 */
			return Message[ioctl_param];
			break;
			//added by swamy
		case IOCTL_GET_PMU_CAPABILITY :

			//cpuid function code taken from Perfmon --swamy, TODO : add license
			cpuid_pmu(0, &a, &b, &c, &d);
			strncpy(&buffer[0], (char *)(&b), 4);
			strncpy(&buffer[4], (char *)(&d), 4);
			strncpy(&buffer[8], (char *)(&c), 4);
			buffer[12] = '\0';

			printk(KERN_INFO "PMU_DEVICE2_SWAMY :: Detected %s\n", buffer); 
			break;
			// added by tim
		case IOCTL_GET_INST_COUNTERS:
			// disable premption while reading counters
			preempt_disable();
			
			//pmc_temp = (pmc_value_t*)ioctl_param;
			
			//TODO why does this cause it to crash?
			// get pmu counter values
			//pmu_read_all_counters(pmc_temp);
			pmu_read_all_counters(&pmc);
			//pmu_read_all_counters(&pmc_test);
			
			// write the value of the pmc couters the buffer specified by ioctl param
			//put_user(pmc, pmc_temp);

			//TODO
			// get current instruction from registers
			regs = task_pt_regs(current);
			inst = regs->ip;
			//current->mm[regs->ip]

			//TODO currently the ip is retrieved, not the instruction
			//TODO I would rather it be the instruction
			//
			//TODO this is good enough because I can print out [ip, instruction]
			//TODO with ptrace

			// test reads are to see how many instructions the measurement code is ~ 74
			// prinkk takes ~ 6000

			// [asm, fixed counters .., programmable counters ..]
			printk(KERN_INFO "%lld,%lld,%lld,%lld,%lld,%lld,%lld,%lld", inst,
					pmc.fc0, pmc.fc1, pmc.fc2, pmc.pc0, pmc.pc1, pmc.pc2, pmc.pc3);
			
			// test second read
			//printk(KERN_INFO "%lld,%lld,%lld,%lld,%lld,%lld,%lld,%lld", inst,
			//		pmc_test.fc0, pmc_test.fc1, pmc_test.fc2, pmc_test.pc0, pmc_test.pc1, 
			//		pmc_test.pc2, pmc_test.pc3);

			preempt_enable();
			break;
		case IOCTL_READ_COUNTERS:
			// I want reading to take as few cycles as possible
			preempt_disable();

			//TODO test the allocation to be sure it works
			//TODO the previous verion caused the driver to have a fault
			//TODO when close is called
			
			//TODO works with KALLOC_BLOCK_N = 1 at granularity ~400

			// allocate more space if space has run out
			if(pmc_node_current == pmc_node_tail){
				pmc_node_current->next = kmalloc(sizeof(pmc_node_t)*KALLOC_BLOCK_N, GFP_KERNEL);

				// set the ->next pointer of all intermediary blocks
				int i;
				pmc_node_t *temp;
				for(i=0; i<KALLOC_BLOCK_N; i++){
					//temp = pmc_node_tail;
					//++pmc_node_tail;
					//temp->next = pmc_node_tail;

					pmc_node_tail = pmc_node_tail->next;
					// because its a pointer, +1 does +sizeof(pmc_node_t);
					pmc_node_tail->next = pmc_node_tail + 1;// + sizeof(pmc_node_t);
				}

				//pmc_node_tail = pmc_node_current->next + (sizeof(pmc_node_t)*KALLOC_BLOCK_N-1);

				// the last one should have a next of 0
				pmc_node_tail->next = 0;
			}
			pmc_node_current = pmc_node_current->next;

			pmu_read_all_counters(&(pmc_node_current->pmc));

			preempt_enable();
			break;
		case IOCTL_SEQUENTIAL_READ:
			preempt_disable();
			//v1 =__rdmsr(INST_RETIRED_ANY_ADDR);
			//v2 =__rdmsr(INST_RETIRED_ANY_ADDR);
			v1 = pmu_read_counter(INST_RETIRED_ANY_ADDR);
			v2 = pmu_read_counter(INST_RETIRED_ANY_ADDR);
			printk(KERN_INFO "sequential read: %lld, %lld", v1, v2);
			preempt_enable();
			break;
		case IOCTL_START_COUNTERS:
			printk(KERN_INFO "start counters");
			pmu_start_all_counters();
			break;
		case IOCTL_STOP_COUNTERS:
			printk(KERN_INFO "stop counters");
			pmu_stop_all_counters();
			break;
		case IOCTL_RESCHEDULE_OTHER:
			// move tasks other than 
			// this task
			// the task running ioctl (pmu.c)
			// to other cpu's
			//preempt_disable();
			cpu_id = get_cpu();

			// ioctl_param is driver pid
			//current->pid
			//printk(KERN_INFO "driver: %d | me: %d", (int)ioctl_param, current->pid);
		
			for_each_process(task){
				// reschedule all other tasks to different cpus
				if(task->pid != current->pid){
					//TODO

					//printk("Name: %s PID: [%d]\n", task->comm, task->pid);
				}
			}		

			//preempt_enable();
			put_cpu();
			break;
		case IOCTL_SET_RT_PRIORITY:
			//TODO
			break;
	}

	return SUCCESS;
}
//task->state /* -1 unrunnable, 0 runnable, >0 stopped */

/* Module Declarations */

/* 
 * This structure will hold the functions to be called
 * when a process does something to the device we
 * created. Since a pointer to this structure is kept in
 * the devices table, it can't be local to
 * init_module. NULL is for unimplemented functions. 
 */
struct file_operations Fops = {
	.read = device_read,
	.write = device_write,
	.unlocked_ioctl = device_ioctl, //changed by swamy
	.open = device_open,
	.release = device_release,	/* a.k.a. close */
};

/* 
 * Initialize the module - Register the character device 
 */
int init_module()
{
	int ret_val;
	/* 
	 * Register the character device (atleast try) 
	 */
	ret_val = register_chrdev(MAJOR_NUM, DEVICE_NAME, &Fops);

	/* 
	 * Negative values signify an error 
	 */
	if (ret_val < 0) {
		printk(KERN_ALERT "%s failed with %d\n",
				"Sorry, registering the character device ", ret_val);
		return ret_val;
	}

	printk(KERN_INFO "%s The major device number is %d.\n",
			"Registeration is a success", MAJOR_NUM);
	printk(KERN_INFO "If you want to talk to the device driver,\n");
	printk(KERN_INFO "you'll have to create a device file. \n");
	printk(KERN_INFO "We suggest you use:\n");
	printk(KERN_INFO "mknod %s c %d 0\n", DEVICE_FILE_NAME, MAJOR_NUM);
	printk(KERN_INFO "The device file name is important, because\n");
	printk(KERN_INFO "the ioctl program assumes that's the\n");
	printk(KERN_INFO "file you'll use.\n");

	return 0;
}

/* 
 * Cleanup - unregister the appropriate file from /proc 
 */
void cleanup_module()
{
	/* 
	 * Unregister the device 
	 */
	unregister_chrdev(MAJOR_NUM, DEVICE_NAME);

}
