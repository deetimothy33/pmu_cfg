/**
 * inline asm to use
 * 	wdmsr
 * 	rdmsr
 * to
 * 	set up pmu
 * 	read events from pmu
 */

typedef struct{
	unsigned long long fc0;
	unsigned long long fc1;
	unsigned long long fc2;
	unsigned long long pc0;
	unsigned long long pc1;
	unsigned long long pc2;
	unsigned long long pc3;
} pmc_value_t;

/**
 * start and stop counters
 *

static inline void pmu_start_fixed_counters(void);
static inline void pmu_stop_fixed_counters(void);
static inline void pmu_start_programmable_counter(int counter, int event);
static inline void pmu_stop_programmable_counter(int counter, int event);
static inline void pmu_start_all_counters(void);
static inline void pmu_stop_all_counters(void);

/**
 * read counters
 *

static inline int pmu_read_fixed_counter(int counter);
static inline int pmu_read_programmable_counter(int counter);
*/
