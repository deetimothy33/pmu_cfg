#include <linux/module.h>
#include <linux/kernel.h>
#include <asm/current.h>
#include <linux/sched.h>

#include <linux/kernel.h>
#include <linux/kthread.h>

#include <linux/perf_event.h>
#include <linux/list.h>

#include <linux/fs.h>

//TODO 
// after activating perf_events subsystem, 
// can I then read out of the task_struct, the event values?

/**
 * syscal to open perf event
 */
static long
perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
		int cpu, int group_fd, unsigned long flags){
	int ret;

	ret = syscall(__NR_perf_event_open, hw_event, pid, cpu,
			group_fd, flags);
	return ret;
}

struct task_struct *task;
int data;
int ret;
int thread_function(void *data){
	int var;
	var = 10;

	// set up perf events
	struct perf_event_attr pe;
	//unsigned long long count;
	int fd;

	memset(&pe, 0, sizeof(struct perf_event_attr));
	pe.type = PERF_TYPE_HARDWARE;
	pe.size = sizeof(struct perf_event_attr);
	pe.config = PERF_COUNT_HW_INSTRUCTIONS;
	pe.disabled = 1;
	pe.exclude_kernel = 1;
	pe.exclude_hv = 1;

	fd = perf_event_open(&pe, 0, -1, -1, 0);
	if (fd == -1) {
		printk(KERN_INFO "Error opening leader %llx\n", pe.config);
		exit(EXIT_FAILURE);
	}

	ioctl(fd, PERF_EVENT_IOC_RESET, 0);
	ioctl(fd, PERF_EVENT_IOC_ENABLE, 0);

	// thread loop
	while(!kthread_should_stop()){
		struct list_head *p, *n;
		struct perf_event *event;
		
		// acquire the context for perf events
		struct perf_event_context* pec[perf_nr_task_contexts];
	        *pec = *(current->perf_event_ctxp);
		//printk(KERN_INFO "nr_events: %d", pec[0]->nr_events);
		
		// log asm instruction and pmu counter values
		// perf event list in task_struct
		list_for_each_safe(p, n, &current->perf_event_list) {
			/* my points to each my_struct in the list */
			event = list_entry(p, struct perf_event, event_entry);

			// prints nothing because nothing is in the list
			printk(KERN_INFO "pid %i | %d\n", current->pid, event->pmu->type);
		}

		// give up processor
		schedule();
	}

	// teardown perf events
	ioctl(fd, PERF_EVENT_IOC_DISABLE, 0);
	//read(fd, &count, sizeof(long long));
	close(fd);

	/*do_exit(1);*/
	return var;
}

int init_module(){
	printk(KERN_INFO "Hello world 1. (pid %i)\n", current->pid);

	// setup PMU
	//TODO

	data = 20;
	//printk(KERN_INFO"--------------------------------------------");
	/*task = kthread_create(&thread_function,(void *)data,"pradeep");*/
	task = kthread_run(&thread_function,(void *)data,"pradeep");
	//printk(KERN_INFO"Kernel Thread : %s\n",task->comm);	

	return 0;
}

void cleanup_module(){
	printk(KERN_INFO "Goodbye world 1.\n");

	kthread_stop(task);

	// shut down PMU
	//TODO
}
