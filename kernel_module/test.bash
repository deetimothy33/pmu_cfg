#! /bin/bash

# load and info
modinfo pmc_log.ko
insmod pmc_log.ko

# test module
#perf record gzip ../monitor/data.txt	
#gunzip ../monitor/data.txt.gz
sleep 1 

# unload and info
rmmod pmc_log.ko
dmesg | tail
