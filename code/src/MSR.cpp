#include "MSR.h"
#include "rdpmc.h" //TODO move to MSR.h file

//TODO replace file reads with wdmsr and rdmsr assembly instructions

//TODO TODO TODO perhaps I should use core affinity and only read the core i'm running on?

#define DUMMY_VALUE 20

MSR::MSR(int cpu){
	this->cpu = cpu;
	//if(enable_rdpmc()) cout << "rdpmc enable success" << endl;
}

/**
 * read the msr register specified in types.h
 * 
 * reg = IA32_DEBUGCTL; => LBR
 */
uint64_t MSR::read(uint32_t reg){
	uint64_t data;
	volatile unsigned a, d, c;
	
	// determine ECX value based on register value
	c = DUMMY_VALUE;
	switch(reg){
		case INST_RETIRED_ANY_ADDR:
			c = FC_0;
			break;
		case CPU_CLK_UNHALTED_THREAD_ADDR:
			c = FC_1;
			break;
		case CPU_CLK_UNHALTED_REF_ADDR:
			c = FC_2;
			break;
		case IA32_PMC0:
			c = PMC_0;
			break;
		case IA32_PMC1:
			c = PMC_1;
			break;
		case IA32_PMC2:
			c = PMC_2;
			break;
		case IA32_PMC3:
			c = PMC_3;
			break;
		//default:
			//TODO not a regsiter readable by rdpmc
			//return (uint64_t)0;
	}

	// rdpmc is used when possible 
	// it will always grab data for current core
	if( c != DUMMY_VALUE ){
		//cout << "rdpmc start: " << c << endl; cout.flush();
		rdpmc(c,a,d);
		//cout << "rdpmc end" << endl; cout.flush();
		
		data = ((unsigned long)a) | (((unsigned long)d) << 32);
	}else{
		int fd;
		char msr_file_name[64];

		sprintf(msr_file_name, "/dev/cpu/%d/msr", this->cpu);

		//cout << msr_file_name << "\n"; cout.flush();

		fd = open(msr_file_name, O_RDONLY);
		if(fd < 0) PRINT_ERROR

		if(pread(fd, &data, sizeof data, reg) < 0){
			std::cout << "data read failure\n"; cout.flush();
			PRINT_ERROR
		}

		close(fd);
	}
	
	return data;
}

/**
 * write the msr wth the specified data
 */
void MSR::write(uint32_t reg, uint64_t data){
	int fd;
	char msr_file_name[64];

	sprintf(msr_file_name, "/dev/cpu/%d/msr", this->cpu);
	fd = open(msr_file_name, O_WRONLY);
	if(fd < 0) PRINT_ERROR
	
	if(pwrite(fd, &data, sizeof data, reg) != sizeof data){
		// failure to write data
		std::cout << "data write failure\n";
		PRINT_ERROR
	}
	
	close(fd);
}

/* write / read registers given a map
 * key = register name
 * value = register value
 * 
 * read will put data into value field
 * write will take data from value field
 * 	and put it in register
 */
void MSR::map_read(std::map<uint32_t, uint64_t>* map){
	std::map<uint32_t, uint64_t>::iterator iterator = map->begin();
	while(iterator != map->end()){
		iterator->second = this->read(iterator->first);
		
		iterator++;
	}
}

void MSR::map_write(std::map<uint32_t, uint64_t>* map){
	std::map<uint32_t, uint64_t>::iterator iterator = map->begin();
	while(iterator != map->end()){
		this->write(iterator->first, iterator->second);
		
		iterator++;
	}	
}

/* print capabilities from capabilities register */
void MSR::print_perf_capabilities(PerfCapabilitiesRegister* reg){
	cout << "MSR core: " << this->cpu << endl;
	cout << "perf capabilities register value: " << reg->value << endl;
	
	switch(reg->fields.lbr_fmt){
		case 0b000000:
			cout << "32-bit code segment" << endl;
			break;
		case 0b000010:
			cout << "64-bit LIP" << endl;
			break;
		case 0b000100:
			cout << "64-bit EIP" << endl;
			break;
		case 0b000110:
			cout << "64-bit EIP and LIP" << endl;
			break;
		default:
			cout << "unknown LBR format: " << reg->fields.lbr_fmt << endl;
	}

	switch(reg->fields.pebs_trap){
		case 0b0:
			cout << "trap-like" << endl;
			break;
		case 0b1:
			cout << "fault-like" << endl;
			break;
	}

	cout << "instruction points and flags are: ";
	switch(reg->fields.pebs_arch_reg){
		case 0b0:
			cout << "not recorded" << endl;
			break;
		case 0b1:
			cout << "recorded" << endl;
			break;
	}

	cout << "pebs record format: " << reg->fields.pebs_rec_fmt << endl;
	cout << "smm freeze capability: " << reg->fields.smm_frz << endl;
}

void MSR::disable(){
	//this->write(IA32_CR_PERF_GLOBAL_CTRL, 0)
		
	std::map<uint32_t, uint64_t> control_map;

	// read current control register	
	control_map[IA32_CR_PERF_GLOBAL_CTRL];
	control_map[IA32_DEBUGCTL];
	this->map_read(&control_map);

	// mask bits to enable everything
	uint64_t mask = 0x000000070000000F;	 
	control_map[IA32_CR_PERF_GLOBAL_CTRL] &= ~mask;

	// disable lbr
	mask = 0x0000000000000001;
	control_map[IA32_DEBUGCTL] &= ~mask;

	// write new value of global control register
	this->map_write(&control_map);
}

void MSR::enable(){
	std::map<uint32_t, uint64_t> control_map;

	// read current control register	
	control_map[IA32_CR_PERF_GLOBAL_CTRL];
	control_map[IA32_DEBUGCTL];
	this->map_read(&control_map);

	// mask bits to enable everything
	uint64_t mask = 0x000000070000000F;	 
	control_map[IA32_CR_PERF_GLOBAL_CTRL] |= mask;

	// enable lbr
	mask = 0x0000000000000001;
	control_map[IA32_DEBUGCTL] |= mask;

	// write new value of global control register
	this->map_write(&control_map);
}

void MSR::setup(programmable_counter_event pce[N_PROGRAMMABLE_COUNTER]){
	this->disable();
	this->setup_fixed_counters();

	//TODO make sure pmc are getting programmed correctly
	//TODO make sure the correct addresses for pmc are getting read
	//TODO it doesn't seem to be reading the programmable counters correctly
	//TODO or perhaps the difference value is actually 0... it is possible
	std::map<uint32_t, uint32_t> value_mask_map;
	for(int i=0; i<N_PROGRAMMABLE_COUNTER; i++){
		switch(pce[i]){
			case MEM_LOAD_RETIRED_L3_MISS:
				value_mask_map[MEM_LOAD_RETIRED_L3_MISS_EVTNR] = MEM_LOAD_RETIRED_L3_MISS_UMASK;
				break;
			case MEM_LOAD_RETIRED_L3_UNSHARED_HIT:
				value_mask_map[MEM_LOAD_RETIRED_L3_UNSHAREDHIT_EVTNR] = 
					MEM_LOAD_RETIRED_L3_UNSHAREDHIT_UMASK;
				break;
			case MEM_LOAD_RETIRED_L2_HITM:
				value_mask_map[MEM_LOAD_RETIRED_L2_HITM_EVTNR] = MEM_LOAD_RETIRED_L2_HITM_UMASK;
				break;
			case MEM_LOAD_RETIRED_L2_HIT:
				value_mask_map[MEM_LOAD_RETIRED_L2_HIT_EVTNR] = MEM_LOAD_RETIRED_L2_HIT_UMASK;
				break;
			case MEM_LOAD_UOPS_MISC_RETIRED_LLC_MISS:
				value_mask_map[MEM_LOAD_UOPS_MISC_RETIRED_LLC_MISS_EVTNR] = 
					MEM_LOAD_UOPS_MISC_RETIRED_LLC_MISS_UMASK;
				break;
			case MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_NONE:
				value_mask_map[MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_NONE_EVTNR] =
					MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_NONE_UMASK;
				break;
			case MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_HITM:
				value_mask_map[MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_HITM_EVTNR] =
					MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_HITM_UMASK;
				break;
			case MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP: 
				value_mask_map[MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_EVTNR] =
					MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_UMASK;
				break;
			case MEM_LOAD_UOPS_RETIRED_L2_HIT:
				value_mask_map[MEM_LOAD_UOPS_RETIRED_L2_HIT_EVTNR] =
					MEM_LOAD_UOPS_RETIRED_L2_HIT_UMASK;
				break;
			case SKL_MEM_LOAD_RETIRED_L3_MISS:
				value_mask_map[SKL_MEM_LOAD_RETIRED_L3_MISS_EVTNR] =
					SKL_MEM_LOAD_RETIRED_L3_MISS_UMASK;
				break;
			case SKL_MEM_LOAD_RETIRED_L3_HIT:
				value_mask_map[SKL_MEM_LOAD_RETIRED_L3_HIT_EVTNR] =
					SKL_MEM_LOAD_RETIRED_L3_HIT_UMASK;
				break;
			case SKL_MEM_LOAD_RETIRED_L2_MISS:
				value_mask_map[SKL_MEM_LOAD_RETIRED_L2_MISS_EVTNR] =
					SKL_MEM_LOAD_RETIRED_L2_MISS_UMASK;
				break;
			case SKL_MEM_LOAD_RETIRED_L2_HIT:
				value_mask_map[SKL_MEM_LOAD_RETIRED_L2_HIT] =
					SKL_MEM_LOAD_RETIRED_L2_HIT_UMASK;
				break;
			case ARCH_LLC_REFERENCE:    
				value_mask_map[ARCH_LLC_REFERENCE_EVTNR] =
					ARCH_LLC_REFERENCE_UMASK;
				break;
			case ARCH_LLC_MISS:     
				value_mask_map[ARCH_LLC_MISS_EVTNR] =
					ARCH_LLC_MISS_UMASK;
				break;
			case ATOM_MEM_LOAD_RETIRED_L2_HIT:
				value_mask_map[ATOM_MEM_LOAD_RETIRED_L2_HIT_EVTNR] =
					ATOM_MEM_LOAD_RETIRED_L2_HIT_UMASK;
				break;
			case ATOM_MEM_LOAD_RETIRED_L2_MISS:
				value_mask_map[ATOM_MEM_LOAD_RETIRED_L2_MISS_EVTNR] =
					ATOM_MEM_LOAD_RETIRED_L2_MISS_UMASK;
				break;
		}
	}
	this->setup_programmable_counters(&value_mask_map);

	this->setup_LBR();
	this->setup_uncore();
	this->enable();
}

void MSR::setup_fixed_counters(){
	std::map<uint32_t, uint64_t> control_map;

	// read current control register	
	control_map[IA32_CR_FIXED_CTR_CTRL];
	this->map_read(&control_map);

	// mask bits to enable everything
	// any priviledge level, only own thread, generate interrupts
	FixedEventControlRegister fec_reg;
	fec_reg.value = control_map[IA32_CR_FIXED_CTR_CTRL];
	fec_reg.fields.os0 = 0b1;
	fec_reg.fields.usr0 = 0b1;
	fec_reg.fields.any_thread0 = 0b0;
	fec_reg.fields.enable_pmi0 = 0b1;
	fec_reg.fields.os1 = 0b1;
	fec_reg.fields.usr1 = 0b1;
	fec_reg.fields.any_thread1 = 0b0;
	fec_reg.fields.enable_pmi1 = 0b1;
	fec_reg.fields.os2 = 0b1;
	fec_reg.fields.usr2 = 0b1;
	fec_reg.fields.any_thread2 = 0b0;
	fec_reg.fields.enable_pmi2 = 0b1;
	control_map[IA32_CR_FIXED_CTR_CTRL] = fec_reg.value;

	// write new value of global control register
	this->map_write(&control_map);
}

void MSR::setup_programmable_counters(std::map<uint32_t, uint32_t>* value_mask_map){
	map<uint32_t, uint32_t>::iterator vm_it = (*value_mask_map).begin();
	std::map<uint32_t, uint64_t> control_map;

	// read current control register	
	for(uint32_t i=0; i<4; i++){ control_map[IA32_PERFEVTSEL0_ADDR + i]; }
	this->map_read(&control_map);

	// for all 4 pmc
	for(uint32_t j=0; j<4; j++){
		EventSelectRegister r;
		r.value = control_map[IA32_PERFEVTSEL0_ADDR + j];

		// event to be counted
		r.fields.event_select = vm_it->first;
		r.fields.umask = vm_it->second;

		r.fields.usr = 1; // count usermode events
		r.fields.os = 1; // count kernelmode events
		r.fields.edge = 1; // increments on de-asserted -> asserted event transition
		//r.fields.pin_control; reserved pin
		r.fields.apic_int = 0; // generate interrupt on overflow
		r.fields.any_thread = 0; // increment on any thread event
		r.fields.enable = 1; // enable counting
		r.fields.invert = 0; // invert when interrupt is generated by cmask
		r.fields.cmask = 0; // ?? set minimum event count per cycle ??
		//r.fields.in_tx = 0; reserved
		//r.fields.in_txcp = 0; reserved 
		//r.fields.reservedX = 0;reserved 
		control_map[IA32_PERFEVTSEL0_ADDR + j] = r.value;

		++vm_it;
	}
	
	// write new value of global control register
	this->map_write(&control_map);
}

//TODO figure out how perf capabilities changes the meaning of LBR read
void MSR::setup_LBR(){
	// setup debugctl register
	std::map<uint32_t, uint64_t> control_map;
	
	// read current value
	control_map[IA32_DEBUGCTL];
	control_map[LBR_SELECT];
	this->map_read(&control_map);

	// mask bits to enable everything
	DebugCtlRegister dcr;
	dcr.value = control_map[IA32_DEBUGCTL];

	dcr.fields.lbr = 1;
	dcr.fields.frz_lbrs_on_pmi = 0;

	control_map[IA32_DEBUGCTL] = dcr.value;

	// clear lbrselect register (no filtering)
	LBRSelectRegister lsr;
	lsr.value = control_map[LBR_SELECT];

	lsr.fields.cpl_eq_0 = 0;
	lsr.fields.cpl_neq_0 = 0;
	lsr.fields.jcc = 0;
	lsr.fields.near_rel_call = 0;
	lsr.fields.near_ind_call = 0;
	lsr.fields.near_ret = 0;
	lsr.fields.near_ind_jmp = 0;
	lsr.fields.near_rel_jmp = 0;
	lsr.fields.far_branch = 0;

	control_map[LBR_SELECT] = lsr.value;
	
	// write new value of global control register
	this->map_write(&control_map);
}

void MSR::setup_uncore(){
	// It appears this will take some doing... 
	// I need to find another programming guide for uncore

	/*
	// setup debugctl register
	std::map<uint32_t, uint64_t> control_map;
	
	// read current value
	control_map[IA32_DEBUGCTL];
	this->map_read(&control_map);

	//TODO does this actually need to be enabled
	//TODO for uncore counters to work?
	// mask bits to enable uncore
	DebugCtlRegister dcr;
	dcr.value = control_map[IA32_DEBUGCTL];
	dcr.fields.uncore_pmi_en = 1;
	control_map[IA32_DEBUGCTL] = dcr.value;

	//TODO		

	// write new value of global control register
	this->map_write(&control_map);
	*/
}

/* create a list of MSR's one for each cpu */
std::vector<MSR*> MSR::create_MSR_vector(){
	// create a set of MSR, one for each processor
	struct dirent **namelist;
	int dir_entries;

	dir_entries = scandir("/dev/cpu", &namelist, Monitor::dir_filter, 0);
	
	// for each cpu
	std::vector<MSR*>* msr_array = new std::vector<MSR*>();
	while(dir_entries--) {
		// create an MSR representation
		msr_array->push_back(new MSR(atoi(namelist[dir_entries]->d_name)));
	}

	if(msr_array->size() == 0) cout << "no msr created: try >$ sudo modprobe msr\n";
	
	return *msr_array;
}

/* test function */
void MSR::test_read(std::vector<MSR*>* msr_array){
	// test using msr
	for(uint64_t i=0; i<msr_array->size(); i++){
		uint64_t data = (*msr_array)[i]->read(IA32_DEBUGCTL);
		std::cout << "msr_value: " << data << std::endl;
	}	
}
