#include <sys/ptrace.h>
#include <signal.h>
//#include <linux/user.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <unistd.h>
#include <stdio.h>
#include <sys/user.h>
#include <sys/reg.h>

/**
 * attaches to a process
 * 
 * question, can I attach to arbitrary process?
 */
int main(){
    
    int  status = 0, pid, addr; 
    struct user_regs_struct uregs;

    //scanf("%x%d", &addr, &pid);
    
    // can I attach to the init process?
    pid = 23664;
    addr = 0;
    
    ptrace(PTRACE_ATTACH, pid, 0, 0);
    wait(&status);
    ptrace(PTRACE_GETREGS, pid, 0, &uregs);
    printf("rip: %x\n", uregs.rip);
    uregs.rip = addr;
    ptrace(PTRACE_SETREGS, pid, 0, &uregs);
    ptrace(PTRACE_CONT, pid, 0, 0);
    wait(&status);
    if(WIFEXITED(status))printf("child over\n");
}

