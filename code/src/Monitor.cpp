#include <iostream>
#include <vector>

#include <dirent.h>

#include "Monitor.h"
#include "MSR.h"
#include "ControlFlowGraph.h"

/**
 * long ptrace(enum __ptrace_request request,
 *          pid_t pid,
 *          void *addr,
 *          void *data);
 * 
 * PTRACE_TRACEME, PTRACE_PEEKTEXT, PTRACE_PEEKDATA, 
 * PTRACE_PEEKUSER, PTRACE_POKETEXT, PTRACE_POKEDATA, 
 * PTRACE_POKEUSER, PTRACE_GETREGS, PTRACE_GETFPREGS, 
 * PTRACE_SETREGS, PTRACE_SETFPREGS, PTRACE_CONT, 
 * PTRACE_SYSCALL, PTRACE_SINGLESTEP, PTRACE_DETACH
 */

/**
 * fork a child,
 * child will exec "program"
 * pareent will monitor the child
 * 
 * return the PID of the child
 */
int Monitor::start(char* program, char** arguments){
	int pid = fork();
	
	// fork returns 0 in the child
	if(pid == 0){
		// child
		// as in CFIMon,
		// use ptrace to enable modifications of the child's state
		ptrace(PTRACE_TRACEME, 0, NULL, NULL);
		
		//execv(program, args);
		execv(program, arguments);
		
		exit(2);
	}else{
		// monitor the process, creating a cfg
		this->monitor(pid, this->cfg);
	}
	
	return pid;
}

/**
 * attach the monitor to an already running program
 * 
 * `man ptrace`
 * Permission to perform a PTRACE_ATTACH is governed by a ptrace
 * access mode PTRACE_MODE_ATTACH_REALCREDS check; see below.
 * 
 * Permission to perform a PTRACE_SEIZE is governed by a ptrace
 * access mode PTRACE_MODE_ATTACH_REALCREDS check; see below.
 * 
 * PTRACE_MODE_REALCREDS
 * Use the caller's real UID and GID or permitted capabilities
 * for LSM checks.  This was effectively the default before Linux
 * 4.5.
 * 
 * PTRACE_MODE_READ
	For "read" operations or other operations that are less
	dangerous, such as: get_robust_list(2); kcmp(2); reading
	/proc/[pid]/auxv, /proc/[pid]/environ, or /proc/[pid]/stat; or
	readlink(2) of a /proc/[pid]/ns/ file.

 * PTRACE_MODE_ATTACH
	For "write" operations, or other operations that are more
	dangerous, such as: ptrace attaching (PTRACE_ATTACH) to
	another process or calling process_vm_writev(2).
	(PTRACE_MODE_ATTACH was effectively the default before Linux
	2.6.27.)
 *
 *  
 */
int Monitor::attach(int pid){
	ptrace(PTRACE_ATTACH, pid, 0, 0);
	
	// transffer control to the monitor
	this->monitor(pid, this->cfg);
	
	// pid is returned only so the interface is consistent
	// with start function
	return pid;
}

/**
 * monitor the pmu values of the child process
 * add values to the CFG
 */
void Monitor::monitor(int pid, ControlFlowGraph* cfg){
	//TODO!!! the msr array needs to be set up in the child
	//TODO use either pipes or sockets to communicate with it?
	//TODO or perhaps shared memory would be a better solution
	//TODO how do I know i'm actually reading the PMU values of the child
	
	// create a set of MSR, one for each processor
	struct dirent **namelist;
	int dir_entries;

	dir_entries = scandir("/dev/cpu", &namelist, Monitor::dir_filter, 0);
	
	// for each cpu
	std::vector<MSR*>* msr_array = new std::vector<MSR*>();
	while(dir_entries--) {
		// create an MSR representation
		msr_array->push_back(new MSR(atoi(namelist[dir_entries]->d_name)));
	}
	
	// test using msr
	for(uint64_t i=0; i<msr_array->size(); i++){
		uint64_t data = (*msr_array)[i]->read(IA32_DEBUGCTL);
		std::cout << "msr_value: " << data << std::endl;
	}
	
	
	// each time the control gets transfered here,
	// I want to update ControlFlowGraph using data in MSR
	cfg->update(msr_array);
	//TODO
	
	//NOTE: ptrace returns -1 on error
	
	// wait for monitored program
	wait(NULL);
	long orig_eax = ptrace(PTRACE_PEEKUSER, pid, 4*11, NULL);
	
	printf("The child made a system call %ld\n", orig_eax);
	
	// stop after 1 instruction
	ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL);
	
	// tell the child to continue executing
	//ptrace(PTRACE_CONT, pid, NULL, NULL);
	
	//TODO
}

/* filter out ".", "..", "microcode" in /dev/cpu */
int Monitor::dir_filter(const struct dirent *dirp){
	if (isdigit(dirp->d_name[0]))
		return 1;
	else
		return 0;
}
