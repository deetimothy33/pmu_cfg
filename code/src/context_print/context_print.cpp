#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <iterator>

#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <inttypes.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>

#include <unistd.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/reg.h>

#include "types.h"
#include "BasicBlock.h"
#include "ControlFlowGraph.h"
#include "Instruction.h"
#include "Monitor.h"
#include "MSR.h"
#include "error.h"

//#include "rdpmc.h"
#include "context.h"
#include "output.h"

/* uncomment to disable noise context for basic block */
#define DISABLE_NOISE 1

/* uncomment to do core agnostic PMU reads (rdpmc isntruction) */
//#define RDPMC_ENABLE 1
//TODO implement code to make this happen

#define LBR_STACK_SIZE 16

/*
 * TODO list
 * - make many different contexts (before context, after context)
 * - run a set of instructions (assembly) between contexs
 * - record PMU deltas every x instructions (print to file)
 * - preform machine learning to see if instructions can be identified within context
 * -- classification problem
 * -- need clean profile of instruction
 * --- NULL context will be used for clean profile
 * -- need "dirty" profile of instruction in context (postives)
 * -- need profile of not instruction in same context (negatives)
 * - perhaps PCA to identify important features also
 *
 * - make sure the "clear()" and "push_back()" do not end up changing the order of the iterator
 */ 

//TODO examine perfmon code to see if they have
//TODO 		a memory agnostic way of accessing msr

//TODO try with spec benchmark... insert call to 
//TODO procedure call that is done conditionally
//TODO condition can be something picked statically or randomly through the context
//TODO then I am dealing with finding my basic block within a real context
//
//TODO look at wdmsr, rdmsr
//TODO look for a core independtant way
//
//TODO what happens to classification when you get further awa from the boundary
//
//TODO paper microarchite excercise to 
//TODO	see what counter events
//TODO 	think about what microarchitectural level count events would
//TODO	expose the information about a given running program



//TODO does disabling and re-enabling lbr have an effect on the values in lbr?
//TODO I have a suspicion that it resets the tos (top of stack)
//
//TODO why is number of correct predictions in LBR always 1 when measured?
//TODO or perhaps LBR is not getting labeled properly

//TODO uncore events

//TODO offcore events

//TODO PEBS events

//TODO load latency, data linear address, data src

//TODO mabe only certain events can be monitored together?

/*
   +---+--------------------+
   | r |    Register(s)     |
   +---+--------------------+
   | a |   %eax, %ax, %al   |
   | b |   %ebx, %bx, %bl   |
   | c |   %ecx, %cx, %cl   |
   | d |   %edx, %dx, %dl   |
   | S |   %esi, %si        |
   | D |   %edi, %di        |
   +---+--------------------+
   */

/**
 * this can be reconfigured
 * to cause the program to measure
 * different events
 */
// define which programmable counter events will be monitored
// definitions of events are in MSR.h
// (pick 4)
// NOTE it will only consider the first 4 given
MSR::programmable_counter_event pce[] = {
	MSR::MEM_LOAD_RETIRED_L3_MISS,
	//MSR::MEM_LOAD_RETIRED_L3_UNSHARED_HIT,
	//MSR::MEM_LOAD_RETIRED_L2_HITM,
	//MSR::MEM_LOAD_RETIRED_L2_HIT,
	//MSR::MEM_LOAD_UOPS_MISC_RETIRED_LLC_MISS,
	MSR::MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_NONE,
	//MSR::MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_HITM,
	//MSR::MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP, 
	MSR::MEM_LOAD_UOPS_RETIRED_L2_HIT,
	//MSR::SKL_MEM_LOAD_RETIRED_L3_MISS,
	MSR::SKL_MEM_LOAD_RETIRED_L3_HIT,
	//MSR::SKL_MEM_LOAD_RETIRED_L2_MISS,
	//MSR::SKL_MEM_LOAD_RETIRED_L2_HIT,
	//MSR::ARCH_LLC_REFERENCE,    
	//MSR::ARCH_LLC_MISS,     
	//MSR::ATOM_MEM_LOAD_RETIRED_L2_HIT,
	//MSR::ATOM_MEM_LOAD_RETIRED_L2_MISS	
};

/**
 * for each msr in msr_array
 * read the pmu registers specified by the map
 */
void read_pmu(std::vector<MSR*>* msr_array, std::map<uint32_t, vector<uint64_t>>* map){
//#ifdef RDPMC_ENABLE
/*
	// put the same value in each of the msr_arry
	// this is fine because we are reading the pmu value for the
	// core this program is running on
	//
	// for each item in the map, read the register, store the value
	//(*msr_array)[i]->map_read(&tmp_map);

	// put the values in tmp_map in to map
	for(std::map<uint32_t, vector<uint64_t>>::iterator it = tmp_map.begin(); it != tmp_map.end(); ++it){
		rdpmc it->first //TODO
		
		for(uint64_t i=0; i<msr_array->size(); i++){
			(it->second)[i] = rdpmc value; //TODO
		}
	}
*/
//#else	
	std::map<uint32_t, uint64_t> tmp_map;

	// put register addresses in to tmp_map
	for(std::map<uint32_t, vector<uint64_t>>::iterator it = map->begin(); it != map->end(); ++it){
		// add register address to tmp_map
		tmp_map[it->first];

		// clear the value vector of map
		it->second.clear();
	}

	// read each msr register in turn
	for(uint64_t i=0; i<msr_array->size(); i++){
		(*msr_array)[i]->map_read(&tmp_map);

		// put the values in tmp_map in to map
		for(std::map<uint32_t, uint64_t>::iterator it = tmp_map.begin(); it != tmp_map.end(); ++it){
			(*map)[it->first].push_back(it->second);
		}
	}
//#endif
}

void write_pmu(std::vector<MSR*>* msr_array, std::map<uint32_t, vector<uint64_t>>* map){
	std::map<uint32_t, uint64_t> tmp_map;

	//TODO for each msr, put values in tmp_map and write
	for(uint64_t i=0; i<msr_array->size(); i++){
		// put the values in tmp_map in to map
		for(std::map<uint32_t, vector<uint64_t>>::iterator it = map->begin(); it != map->end(); ++it){
			tmp_map[it->first] = it->second[i];
		}
		
		(*msr_array)[i]->map_write(&tmp_map);
	
	}
}

/**
 * initial setup of PMU
 * return a map of values which likke be measured
 * 
 * register is the key, value is the value
 * values returned from this are all null
 * 
 * the point is to provide values for
 * registers which should be read
 * 
 * control_map is used to write the control registers
 * to values needed to read those in value map
 */
//TODO move control map programming into MSR, this will make it easy to use anywhere, and also to re-program
void setup_pmu(std::vector<MSR*>* msr_vector, std::map<uint32_t, vector<uint64_t>>* value_map,
		std::map<uint32_t, vector<uint64_t>>* lbr_map){	
	std::map<uint32_t, vector<uint64_t>> info_map;
	std::map<uint32_t, vector<uint64_t>> control_map;

	// read and print perf capabilities register
	info_map[IA32_PERF_CAPABILITIES];	
	read_pmu(msr_vector, &info_map);
	for(uint64_t i=0; i<1; i++){ //msr_vector->size(); i++){
		(*msr_vector)[i]->print_perf_capabilities(
				(PerfCapabilitiesRegister*)&(info_map[IA32_PERF_CAPABILITIES][i]));
	}
	cout << endl;

	// ask each MSR to perform its setup routine
	for(uint64_t i=0; i<msr_vector->size(); i++){ (*msr_vector)[i]->setup(pce); }

	// setup value_map, fixed counters
	// this entails defining value_map[ctrl_register]
	(*value_map)[INST_RETIRED_ANY_ADDR];
	(*value_map)[CPU_CLK_UNHALTED_THREAD_ADDR];
	(*value_map)[CPU_CLK_UNHALTED_REF_ADDR];

	// setup value map, programmable counters	
	for(uint32_t i=0; i<4; i++){ (*value_map)[IA32_PMC0 + i]; }

	//TODO perhaps it would be better to read LBR separately?
	//TODO computing the difference on LBR registers is meaningless
	
	// setup value map, LBR registers
	(*lbr_map)[MSR_LASTBRANCH_TOS];	
	for(uint32_t i=0; i<16; i++){ 
		(*lbr_map)[MSR_LASTBRANCH_FROM_IP + i]; 
		(*lbr_map)[MSR_LASTBRANCH_TO_IP + i]; 
	}
}



/**
 * compute the difference between the values of m0 and m1
 */
//TODO does this work? (it seems to)
void compute_delta(std::map<uint32_t, vector<uint64_t>>* m0, std::map<uint32_t, vector<uint64_t>>* m1,
			std::map<uint32_t, vector<uint64_t>>* delta){
		// iterate over the k,values in the maps
		// put the difference into delta map
		//
		// for each thing in m0
		for(std::map<uint32_t, vector<uint64_t>>::iterator it0 = m0->begin(); it0 != m0->end(); ++it0){
			std::map<uint32_t, vector<uint64_t>>::iterator it1 = m1->find(it0->first);

			// if the thing is also found in m1
			if(it1 != m1->end()){
				// add the vector difference to delta map
				(*delta)[it0->first].clear();

				for(uint64_t i=0; i<it0->second.size(); i++){
					(*delta)[it0->first].push_back(it1->second[i] - it0->second[i]);
				}
			}
		}
}

/**
 * helper method to compute lbr statistics for a single msr
 */
//TODO make sure this works
void compute_lbr_stat_msr(map<uint32_t, uint64_t>* lbr_map, map<string, uint64_t>* stat_map){
	// read top of stack register
	LastBranchTOSRegister tos;
       	tos.value = (*lbr_map)[MSR_LASTBRANCH_TOS];

	// read the lbr in order from newest to oldest
	LastBranchRegister* from_lbr_array = new LastBranchRegister[LBR_STACK_SIZE];
	LastBranchRegister* to_lbr_array = new LastBranchRegister[LBR_STACK_SIZE];

	for(int i=0; i<LBR_STACK_SIZE; i++){
		int index = (tos.fields.tos + i)%16;

		from_lbr_array[i].value = (*lbr_map)[MSR_LASTBRANCH_FROM_IP + index];
		to_lbr_array[i].value = (*lbr_map)[MSR_LASTBRANCH_TO_IP + index];
	}

	// 0 is newest lbr pair
	// 15 is oldest lbr pair
	
	// compute lbr statistics { average, number of correct predictions in lbr}
	// 
	// only consider the lowest 8 bits of ip address
	uint64_t ip_mask = 0x00000000000000FF;
	
	uint64_t sum_from = 0;
	uint64_t sum_to = 0;
	uint64_t n_correct_prediction = 0;
	for(int i=0; i<LBR_STACK_SIZE; i++){
		sum_from += from_lbr_array[i].fields.data & ip_mask;
		sum_to += to_lbr_array[i].fields.data & ip_mask;

		// set implies correct prediction
		n_correct_prediction += from_lbr_array[i].fields.mispred;
	}
	
	(*stat_map)["average_from"] = sum_from / LBR_STACK_SIZE;
	(*stat_map)["average_to"] = sum_to / LBR_STACK_SIZE;
	(*stat_map)["n_correct_prediction"] = n_correct_prediction;

	//TODO add additional statistics from LBR
}

//TODO make sure this works
void compute_lbr_stat(std::map<uint32_t, vector<uint64_t>>* lbr_map, 
		std::map<string, vector<uint64_t>>* lbr_stat_map){
	uint64_t msr_count = lbr_map->begin()->second.size();

	// clear the old values in the stat map
	lbr_stat_map->clear();

	// compute stats for each msr
	for(uint64_t i=0; i<msr_count; i++){
		map<string, uint64_t> stat_map;
		map<uint32_t, uint64_t> msr_lbr_map;

		// populate the msr_lbr_map for this msr
		for(map<uint32_t, vector<uint64_t>>::iterator it = lbr_map->begin(); it != lbr_map->end(); ++it){
			msr_lbr_map[it->first] = (*lbr_map)[it->first][i];
		}

		// compute stats for this msr
		compute_lbr_stat_msr(&msr_lbr_map, &stat_map);

		// populate values in the lbr_stat_map
		// with values from stat_map
		for(map<string, uint64_t>::iterator it = stat_map.begin(); it != stat_map.end(); ++it){
			// ensure there is a vector
			if((*lbr_stat_map).find(it->first) == lbr_stat_map->end()){
				// if it doesn't exist, create it
				(*lbr_stat_map)[it->first] = vector<uint64_t>(msr_count);
			}


			(*lbr_stat_map)[it->first][i] = it->second;
		}
	}
}


//TODO is there a way to limit execution to one core?

/*
 * goal: set up measurement loop
 * 
 * argv[1] = number of measurements
 * argv[2] = output file name
 * argv[3] = context seed for function
 */
int main(int argc, char **argv){
	std::map<uint32_t, vector<uint64_t>> delta_map;
	std::map<uint32_t, vector<uint64_t>> pmu_value_map_0;	
	std::map<uint32_t, vector<uint64_t>> pmu_value_map_1;	
	std::map<uint32_t, vector<uint64_t>> lbr_map;	
	std::map<string, vector<uint64_t>> lbr_stat_map;	
	
	bool basic_block_executed;

	// create MSR list
	std::vector<MSR*> msr_vector = MSR::create_MSR_vector();

	// setup pmu to measure
	// return map contains keys as registers
	// for values that will be monitored
#ifdef RDPMC_ENABLE
	//TODO
#else
	setup_pmu(&msr_vector, &pmu_value_map_0, &lbr_map);
#endif
	
	// operator= copies container content in c++ maps
	pmu_value_map_1 = pmu_value_map_0;
	
	// reset the output file
	reset_output_file(argv[2]);

	// take an argument from the command line
	// as the number of measurement loops to run
	//int a = 10, c; //, b = 20, d = 13, c;
	//int* a_p = &a;
	
	//TODO my goal is to discover how small of an array I can classify correctly
	//TODO and which PMU counter values are best at doing it
	int array_size = 1000;
	int a[array_size], b[array_size], c[array_size];

	int i = 0;
	while(i < atoi(argv[1])){
		// reset basic block values
		for(int i=0; i<array_size; i++){
			a[i] = (i * 27 + 56) / 30;
			b[i] = (i * 123 + 77) / 4;
		}

		// determine whether to run the basic block or not
		basic_block_executed = (i % 2) == 0;

		// get initial pmu values
		read_pmu(&msr_vector, &pmu_value_map_0);
	
		// enable pmu
		for(vector<MSR*>::iterator it = msr_vector.begin(); it != msr_vector.end(); ++it) (*it)->enable();

		// create a context for the function
#ifndef DISABLE_NOISE
		create_context(atoi(argv[3]), false);
#endif

		//TODO include memory operands... this will allow 
		//TODO look for branch things
		//TODO load,store -- alu -- branch
		// LBR information
		if(basic_block_executed){
			// run the "basic block" I want to classify
			for(int i=0; i<array_size; i++){
				c[i] = a[i] * b[i];		
			}	

			// use a memory operation to make this
			// potentially affect counters counting memory things 
			/*
			asm volatile(
					"movl %2, %%ebx;"
					"idiv dword [%%ebx];"
					"movl %1, %0;"
					: "=r" (c)
					: "r" (a), "r" (a_p)
				    );
			*/

			/*
			asm volatile(
			"movl %1, %%ebx;"
			"addl %2, %%ebx;" // add a, b
			"xorl %3, %%ebx;" // (a+b) xor d
			"movl %1, %0;" // c = a
			"imul %%ebx, %0;" // c = (a+b xor d) * a
			: "=r" ( c )        // outputs %0 
			: "r" ( a ), "r" (b), "r" (d)   // inputs %1 
			: "%ebx" 			// clobbered registers 
			);
			*/
		}
	
		// create a context for the function
		#ifndef DISABLE_NOISE
		create_context(atoi(argv[3]), true);
		#endif

		// disable pmu
		for(vector<MSR*>::iterator it = msr_vector.begin(); it != msr_vector.end(); ++it) (*it)->disable();
		
		// read final pmu values
		read_pmu(&msr_vector, &pmu_value_map_1);
		compute_delta(&pmu_value_map_0, &pmu_value_map_1, &delta_map);

		// read LBR, compute lbr_stats
		read_pmu(&msr_vector, &lbr_map);
		compute_lbr_stat(&lbr_map, &lbr_stat_map);
		
		// log pmu delta values to a file
		log_pmu_delta(argv[2], delta_map, lbr_stat_map, basic_block_executed);
		
		i++;
	}
	
	return 0;
}

/**
 * references
 * 	
 * rdpmc
 * 	http://flint.cs.yale.edu/cs421/papers/x86-asm/asm.html
 * 	*** https://software.intel.com/sites/products/collateral/hpc/vtune/performance_analysis_guide.pdf
 *
 * context switching and PMU counters
 * 	http://stackoverflow.com/questions/38848914/pmu-for-multi-threaded-environment
 *
 */
