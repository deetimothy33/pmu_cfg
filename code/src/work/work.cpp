#include <iostream>

#include <unistd.h>

/*
 * preforms some work
 * This will be called as a dummy program to monitor
 */
int main(){
	int i = 0;
	
	while(i < 10){
		std::cout << "i: " << i << std::endl;
		sleep(1);
		
		i++;
	}
}
