#include <iostream>
#include <string>

#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <inttypes.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>

#include <unistd.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/reg.h>

#include "types.h"
#include "BasicBlock.h"
#include "ControlFlowGraph.h"
#include "Instruction.h"
#include "Monitor.h"

#define print_status(err) if(err != 0) std::cout << "error: " << err << std::endl;
#define print_error() if(errno != 0) perror((const char*)errno);

/*
 * TODO list
 * - how to get ptrace to stop after every instruction of the child
 * - can arbitrary processes be traced with ptrace()?
 * - be sure to use volatile keyword to forse reads on PMU
 * - asm for inline assembly
 */ 

/*
 * goal: set up pmu, fork a child, child exec's, monitor pmu values of child
 */
int main(int argc, char **argv){
	Monitor* monitor = new Monitor();
	
	// start the process to be monitored
	char* args[] = {"", NULL};
	int pid = monitor->start("bin/work", args);
	
	int status;
	while(waitpid(pid, &status, WNOHANG) == 0){
		sleep(1);
	}
	
	return status;
}
