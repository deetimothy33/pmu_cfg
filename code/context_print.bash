#!/bin/bash

# ensure nmi watchdog is disabled
sudo ./disable_nmi_watchdog.bash
sudo ./enable_rdpmc.bash
sudo modprobe msr

# cannot read msr without sudo
#sudo bin/context_print 10 output/context_print.csv 0
#sudo bin/context_print 500 output/context_print.csv 0
sudo bin/context_print 1000 output/context_print.csv 0

sleep 2 

# print output file showing PMU deltas
cat output/context_print.csv
