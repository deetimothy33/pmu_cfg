#include <cerrno>
#include <cstring>
#include <errno.h>

// print out errno
#define PRINT_ERROR std::cout << "errno " << errno << ": " << std::strerror(errno) << std::endl; std::cout.flush();
#define print_status(err) if(err != 0){ std::cout << "error: " << err << std::endl; std::cout.flush(); }

// print out a message and flush output, useful in testing for segfaults
#define PRINT std::cout << "-------------------------------------------------\n"; std::cout.flush();
