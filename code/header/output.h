#ifndef OUTPUT_H
#define OUTPUT_H

/**
 * translate the register addresses into strings for printing
 */
string address_to_string(uint32_t address){
	switch(address){
		case INST_RETIRED_ANY_ADDR: return "inst_retired_any";
		case MEM_INST_RETIRED: return "mem_inst_retired";
		case CPU_CLK_UNHALTED_THREAD_ADDR: return "cpu_clk_unhalted_thread";
		case CPU_CLK_UNHALTED_REF_ADDR: return "cpu_clk_unhalted_ref";
		case IA32_PMC0: return "pmc0";
		case IA32_PMC1: return "pmc1";
		case IA32_PMC2: return "pmc2";
		case IA32_PMC3: return "pmc3";
	}

	return "";
}

/**
 * output the current pmu delta values
 * need to print
 * - PMU delta
 * - context of instruction
 */
static bool first_log = true;
void log_pmu_delta(char* file_name, std::map<uint32_t, vector<uint64_t>> delta_map,
	       std::map<string, vector<uint64_t>> lbr_stat_map, bool basic_block_executed){
	std::ofstream file(file_name, std::ofstream::app);
	
	// write headers from delta_map
	// write values from delta_map
	// get and write context information
	// ... simply represent context_before, context_after with numbers?"

	//TODO TODO TODO this is my best idea about how to do this so far..... utilize it
	//TODO I could classify the basic block as simply being there or not there
	//TODO this would requre that sometimes I run the basic block inbetween the context
	//TODO and sometimes I do not run the basic block inbetween the contexts
	//
	//TODO tldr: classify whethere the basic block was run {yes => run, no => not run}
		
	// if this is the first log to the file
	// output column headers
	if(first_log){
		file << "context_before,";
		file << "context_after,";
		file << "basic_block_executed";
		
		// for each register in delta_map
		for(map<uint32_t, vector<uint64_t>>::iterator it = delta_map.begin(); it != delta_map.end(); ++it){
			string register_name = address_to_string(it->first);

			// for each msr value in the vector
			int i=0;
			for(vector<uint64_t>::iterator v_it = it->second.begin(); v_it != it->second.end(); ++v_it){
				file << "," << register_name << "_" << i;
				i++;
			}
		}

		// for each lbr statistic in lbr_stat_map
		for(std::map<string, vector<uint64_t>>::iterator it = lbr_stat_map.begin();
				it != lbr_stat_map.end(); ++it){
			string stat_name = it->first;
			
			// for each msr value in the vector
			int i=0;
			for(vector<uint64_t>::iterator v_it = it->second.begin(); v_it != it->second.end(); ++v_it){
				file << "," << stat_name << "_" << i;
				i++;
			}
		}
		//TODO

		//file << ",pmu_value...,";		
		file << "\n";

		first_log = false;
	}
	
	// print out data line
	int context_before = 0;
	int context_after = 1;

	file << context_before << "," << context_after << "," << basic_block_executed;

	// for each register in delta_map
	for(map<uint32_t, vector<uint64_t>>::iterator it = delta_map.begin(); it != delta_map.end(); ++it){
		// for each msr value in the vector
		for(vector<uint64_t>::iterator v_it = it->second.begin(); v_it != it->second.end(); ++v_it){
			file << "," << *v_it;
		}
	}

	// for each lbr statistic
	for(std::map<string, vector<uint64_t>>::iterator it = lbr_stat_map.begin();
			it != lbr_stat_map.end(); ++it){
		// for each msr value in the vector
		for(vector<uint64_t>::iterator v_it = it->second.begin(); v_it != it->second.end(); ++v_it){
			file << "," << *v_it;
		}
	}	

	file << "\n";
	
	file.close();
}

/**
 * clear the output file
 */
void reset_output_file(char* file_name){
	std::ofstream file(file_name, std::ofstream::trunc);
	
	file.close();
}

#endif
