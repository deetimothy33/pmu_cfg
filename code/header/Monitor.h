#ifndef MONITOR_H
#define MONITOR_H

#include <iostream>
#include <string>

#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <inttypes.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>

#include <unistd.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/reg.h>

#include "types.h"
//#include "BasicBlock.h"
//#include "Instruction.h"
#include "ControlFlowGraph.h"

using namespace std;

class ControlFlowGraph;

class Monitor{
	public:
		ControlFlowGraph* cfg;
	
		int start(char* program, char** arguments);
		int attach(int pid);
		static int dir_filter(const struct dirent *dirp);
		
		Monitor(){
			//TODO
			//this->cfg = new ControlFlowGraph();
		}
	
	private:
		void monitor(int pid, ControlFlowGraph* cfg);
};

#endif
