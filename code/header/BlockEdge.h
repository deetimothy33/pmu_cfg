#ifndef BLOCK_EDGE_H
#define BLOCK_EDGE_H

#include "BasicBlock.h"

class BlockEdge{
	public:
		BasicBlock* begin;
		BasicBlock* end;
		
};

#endif
