#ifndef CFG_H
#define CFG_H

#include <set>
#include <vector>

#include "BasicBlock.h"
#include "BlockEdge.h"
#include "MSR.h"

class MSR;

class ControlFlowGraph{
	public:
		std::set<BasicBlock>* basic_block_set;
		std::set<BlockEdge>* edge_set;
		
		void update(std::vector<MSR*>* msr_array);
		
		ControlFlowGraph(){
			this->basic_block_set = new std::set<BasicBlock>();
			this->edge_set = new std::set<BlockEdge>();
		}
};

#endif
