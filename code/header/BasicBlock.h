#ifndef BASIC_BLOCK_H
#define BASIC_BLOCK_H

#include <list>

#include "Instruction.h"

class BasicBlock{
	public:
		uint64_t address;
		std::list<Instruction> instruction_list;
};

#endif
