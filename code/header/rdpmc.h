#pragma once
#ifndef RDPMC_H
#define RDPMC_H

/**
 * Example usage of rdpmc instruction
 *
 * source:
 * https://software.intel.com/en-us/forums/software-tuning-performance-optimization-platform-monitoring/topic/595214
 */

//#define _GNU_SOURCE
#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sched.h>
#include <string.h>
#include <errno.h>

#define FATAL(fmt,args...) do {                \
    ERROR(fmt, ##args);                        \
    exit(1);                                   \
  } while (0)

#define ERROR(fmt,args...) \
    fprintf(stderr, fmt, ##args)

#define rdpmc(counter,low,high) \
     __asm__ __volatile__("rdpmc" \
        : "=a" (low), "=d" (high) \
        : "c" (counter))

using namespace std;

// enumerate the possible counter values
enum counter_ecx{
	PMC_0 = 0,
	PMC_1,
	PMC_2,
	PMC_3,
	FC_0 = 0x40000000,
	FC_1,
	FC_2
};

//TODO move to .cpp file

int cpu, nr_cpus;

/**
 * enables rdpmc instruction
 *
 * returns true if successful
 */
//TODO causes segfault
bool enable_rdpmc(){
	uint64_t output;
	
	// Set CR4, Bit 8  to enable PMC
	__asm__("push   %rax\n\t"
			"mov    %cr4,%rax;\n\t"
			"or     $(1 << 8),%rax;\n\t"
			"mov    %rax,%cr4;\n\t"
			"wbinvd\n\t"
			"pop    %rax"
	       );

	// Read back CR4 to check the bit.
	__asm__("\t mov %%cr4,%0" : "=r"(output));

	return output & (1<<8);
}

void handle ( int sig )
{
  FATAL("cpu %d: caught %d\n", cpu, sig);
}

int example_main ( int argc, char *argv[] )
{
  nr_cpus = sysconf(_SC_NPROCESSORS_ONLN);
  for (cpu = 0; cpu < nr_cpus; cpu++) {

    pid_t pid = fork();
    if (pid == 0) {
      cpu_set_t cpu_set;
      CPU_ZERO(&cpu_set);
      CPU_SET(cpu, &cpu_set);
      if (sched_setaffinity(pid, sizeof(cpu_set), &cpu_set) < 0)
        FATAL("cannot set cpu affinity: %m\n");

      signal(SIGSEGV, &handle);

      unsigned int low, high;
      rdpmc(0, low, high);

      ERROR("cpu %d: low %u, high %u\n", cpu, low, high);
      break;
    }
  }

  return 0;
}


// rdpmc_instructions uses a "fixed-function" performance counter to return the count of retired instructions on
//       the current core in the low-order 48 bits of an unsigned 64-bit integer.
unsigned long rdpmc_instructions()
{
   unsigned a, d, c;

   c = (1<<30);
   __asm__ volatile("rdpmc" : "=a" (a), "=d" (d) : "c" (c));

   return ((unsigned long)a) | (((unsigned long)d) << 32);;
}

// rdpmc_actual_cycles uses a "fixed-function" performance counter to return the count of actual CPU core cycles
//       executed by the current core.  Core cycles are not accumulated while the processor is in the "HALT" state,
//       which is used when the operating system has no task(s) to run on a processor core.
unsigned long rdpmc_actual_cycles()
{
   unsigned a, d, c;

   c = (1<<30)+1;
   __asm__ volatile("rdpmc" : "=a" (a), "=d" (d) : "c" (c));

   return ((unsigned long)a) | (((unsigned long)d) << 32);;
}

// rdpmc_reference_cycles uses a "fixed-function" performance counter to return the count of "reference" (or "nominal")
//       CPU core cycles executed by the current core.  This counts at the same rate as the TSC, but does not count
//       when the core is in the "HALT" state.  If a timed section of code shows a larger change in TSC than in
//       rdpmc_reference_cycles, the processor probably spent some time in a HALT state.
unsigned long rdpmc_reference_cycles()
{
   unsigned a, d, c;

   c = (1<<30)+2;
   __asm__ volatile("rdpmc" : "=a" (a), "=d" (d) : "c" (c));

   return ((unsigned long)a) | (((unsigned long)d) << 32);;
}

#endif
