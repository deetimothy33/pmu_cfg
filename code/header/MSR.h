#ifndef MSR_H
#define MSR_H

#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <map>
#include <cerrno>
#include <cstring>

#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <inttypes.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>

#include <unistd.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/reg.h>

#include "types.h"
#include "Monitor.h"
//#include "BasicBlock.h"
//#include "ControlFlowGraph.h"
//#include "Instruction.h"
#include "error.h"
//#include "rdpmc.h" //TODO

/* uncomment to do core agnostic PMU reads (rdpmc isntruction) */
//#define RDPMC_ENABLE 1

#define N_PROGRAMMABLE_COUNTER 4

/**
 * register definities are in types.h
 *
 * define additional register register locations
 */
/**
 * value registers
 */
#define MEM_INST_RETIRED (0x0B)
#define MEM_INST_RETIRED_LOAD_MASK (0x01)
#define MEM_INST_RETIRED_STORE_MASK (0x02)
#define MEM_INST_RETIRED_LATENCY_ABOVE_THRESHOLD_MASK (0x10)

/**
 * control registers
 */
#define IA32_PERF_CAPABILITIES (0x345)
struct PerfCapabilitiesRegister{
    union{
        struct{
            uint64 lbr_fmt : 6;
            uint64 pebs_trap : 1;
            uint64 pebs_arch_reg : 1;
            uint64 pebs_rec_fmt : 4;
            uint64 smm_frz : 1;
            uint64 reservedX : 51;
        } fields;
        uint64 value;
    };
};

struct DebugCtlRegister{
	union{
		struct{
			uint64 lbr : 1;
			uint64 btf : 1;
			uint64 reserved_0 : 4;
			uint64 tr : 1;
			uint64 bts : 1;
			uint64 btint : 1;
			uint64 bts_off_os : 1;
			uint64 bts_off_usr : 1;
			uint64 frz_lbrs_on_pmi : 1;
			uint64 frz_perfmon_on_pmi : 1;
			uint64 uncore_pmi_en : 1;
			uint64 smm_frz : 1;
			uint64 reserved_1 : 49;
		} fields;
		uint64 value;
	};
};

// implements Filtering LBR,
// setting a bit causes that information not to be recorded
#define LBR_SELECT 0x1C8
struct LBRSelectRegister{
	union{
		struct{
			uint64 cpl_eq_0 : 1;
			uint64 cpl_neq_0 : 1;
			uint64 jcc : 1;
			uint64 near_rel_call : 1;
			uint64 near_ind_call : 1;
			uint64 near_ret : 1;
			uint64 near_ind_jmp : 1;
			uint64 near_rel_jmp : 1;
			uint64 far_branch : 1;
			uint64 reserved : 55;
		} fields;
		uint64 value;
	};
};

/**
 * PEBS events
 */
#define IA32_PEBS_ENABLE (0x3F1)

/**
 * miscelanious registers
 */
#define IA32_MISC_ENABLE (0X1A0)

/**
 * LBR registers
 *
 * there are 16 of each 
 * 	from: (0x680 - 0x68F) 
 * 	to: (0x6C0 - 0x6CF)
 * these addresses are the beginning
 */
#define MSR_LASTBRANCH_FROM_IP 0x680
#define MSR_LASTBRANCH_TO_IP 0x6C0
// mispred bit is only in the from register!
struct LastBranchRegister{
	union{
		struct{
			uint64 data : 48;
			uint64 sign_ext : 15;
			uint64 mispred : 1; // set => branch predicted correctly
		} fields;
		uint64 value;
	};
};

// tells you where to begin reading in LBR stack
// read beginning 
#define MSR_LASTBRANCH_TOS 0x1C9
struct LastBranchTOSRegister{
	union{
		struct{
			uint64 tos : 4;
			uint64 reserved : 60;
		} fields;
		uint64 value;
	};
};

using namespace std;

class MSR{
	public:
		enum programmable_counter_event{
			MEM_LOAD_RETIRED_L3_MISS,
			MEM_LOAD_RETIRED_L3_UNSHARED_HIT,
			MEM_LOAD_RETIRED_L2_HITM,
			MEM_LOAD_RETIRED_L2_HIT,
			MEM_LOAD_UOPS_MISC_RETIRED_LLC_MISS,
			MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_NONE,
			MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP_HITM,
			MEM_LOAD_UOPS_LLC_HIT_RETIRED_XSNP, 
			MEM_LOAD_UOPS_RETIRED_L2_HIT,
			SKL_MEM_LOAD_RETIRED_L3_MISS,
			SKL_MEM_LOAD_RETIRED_L3_HIT,
			SKL_MEM_LOAD_RETIRED_L2_MISS,
			SKL_MEM_LOAD_RETIRED_L2_HIT,
			ARCH_LLC_REFERENCE,    
			ARCH_LLC_MISS,     
			ATOM_MEM_LOAD_RETIRED_L2_HIT,
			ATOM_MEM_LOAD_RETIRED_L2_MISS
		};

		//TODO make a similar enum for uncore events

		int cpu;
		
		/* add functions to read specific data from regsiters */
		uint64_t read(uint32_t reg);
		
		/* add functions to write specific data to regsiters */
		void write(uint32_t reg, uint64_t data);
		
		/* write / read registers given a map
		 * key = register name
		 * value = register value
		 * 
		 * read will put data into value field
		 * write will take data from value field
		 * 	and put it in register
		 */
		void map_read(std::map<uint32_t, uint64_t>* map);
		void map_write(std::map<uint32_t, uint64_t>* map);

		/* print capabilities from capabilities register */
		void print_perf_capabilities(PerfCapabilitiesRegister* reg);

		/* disable using global control register */
		void disable();

		/* enable using global control register */
		void enable();

		/* setup fixed and programmable counters with default values */
		void setup(programmable_counter_event pce[N_PROGRAMMABLE_COUNTER]);

		/* setup fixed counters */
		void setup_fixed_counters();

		/* setup programmable counters */
		void setup_programmable_counters(std::map<uint32_t, uint32_t>* value_mask_map);

		/* setup LBR facility */
		void setup_LBR();

		/* setup uncore events */
		void setup_uncore();

		/* create a list of MSR's one for each cpu */
		static std::vector<MSR*> create_MSR_vector();
		
		/* test function */
		static void test_read(std::vector<MSR*>* msr_array);
		
		MSR(int cpu);
};

#endif
