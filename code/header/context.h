#ifndef CONTEXT_H
#define CONTEXT_H

//TODO make a better version of noise which
//TODO inserts one basic block
/**
 * create a context for the function.
 *
 * TODO what should the context be?
 * . always the same?
 * . different context each time?
 * . same for a given seed value?
 *
 * execute a random number of random instructions
 * this is equivilent to introducing noise
 */
//TODO should context be a random number of random instructions ro something else
void create_context(int seed, bool after){
	//TODO does this do what I think it does?
	//TODO I want a seed that is different if ether changes
	srand(seed & after);

	int n_inst = rand() % 50 + 1;
	int a=0,b=1,c=2,d=3,e=4,f=5,g=0;
	//double h=0,i=1,j=2,k=3,l=4,m=5,n=0;
	for(int i=0; i<n_inst; i++){
		// execute a meaningless instruction
		switch(rand() % 12){
			case 0: g = c*d; break;
			case 1: g = d*e; break;
			case 2: g = e*f; break;
			case 3: g = a*b; break;
			case 4: g = b*c; break;
			case 5: g = a+b; break;
			case 6: g = b+c; break;
			case 7: g = c+d; break;
			case 8: g = d+e; break;
			case 9: g = e+f; break;
			case 10: g = g*g; break;
			case 11: g = g+g; break;
		}
	}
}

#endif
