#! /bin/python2.7

# import the path to the perf python scripts
#import sys
#sys.path.insert(0, '/usr/lib/perf/scripts/python/')

import event_analyzing_sample as ea
import csv
import sys

###
# To fix: export PERF_EXEC_PATH=/usr/lib/perf
# run with: sudo perf script -s print_database.py
###

###
# print the database created by ./create_database.bash
#   to stdout
#   to csv file
###

def clear_database():
    print 'clearing database content'

    ea.con.execute("drop table gen_events")
    ea.con.execute("drop table pebs_ll")

def print_database():
    print 'printing database content'
    #print ea.show_general_events()
    #print ea.show_pebs_ll()

    # get database contents
    data = ea.con.execute("select * from gen_events")

    # print database contents
    for row in data:
        print "%s %s %s %s" % (row[0], row[1], row[2], row[3])
        #print "%s %s" % (row[0], row[1])

def main():
    print_database()
    #clear_database()
    
    # close sql client connection
    ea.con.close()
    
    # redirect stdout
    sys.stdout = open('/dev/null', 'w')


if __name__ == '__main__':
    main()
