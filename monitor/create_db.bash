#! /bin/bash

PROGRAM=gzip/gzip-1.8/gzip
#PROGRAM=gzip

# record program
# specify events
dd if=/dev/urandom of=data.txt bs=1024 count=10
#sudo perf record -e intel_pt//pp $PROGRAM data.txt
sudo perf record $PROGRAM data.txt
gunzip data.txt.gz

# create database
#sudo perf script -s /usr/lib/perf/scripts/python/event_analyzing_sample.py
sudo perf script -s event_analyzing_sample.py

# print database to csv file
export PERF_EXEC_PATH=/usr/lib/perf
sudo perf script -s print_database.py

# deprecated
#sudo perf script -s /usr/lib/perf/scripts/python/export-to-postgresql.py db branches calls
