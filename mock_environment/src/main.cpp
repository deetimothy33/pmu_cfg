#include <iostream>
#include <queue>
#include <vector>
#include <functional> 
#include <string>

#include "/opt/pugixml/src/pugixml.hpp"

using namespace std;
using namespace pugi;

//TODO see if problem, resource, instruction   sets might be used here
//TODO see if a program can be revers engineered using this method

//TODO the problems are probableistic.... not guarenteed
//TODO how to model and reverse engineer the program in this case

//TODO describe how the dependencies interact
//TODO the dependencies should end up modifying the counters somehow

class instruction{
	public:
		string type;
		string src0;
		string src1;
		string dest;

		int cycles;

		instruction(){}

	private:
};

class pmu{
	public:
		vector<int> count_vector;
		vector<string> pmc_name;
		
		pmu(){
			// create a number of pmc
			// the return value of the pmc is the amount to increase the count
			pmc_name.push_back("inst type printer");
			pmc_vector.push_back([&](instruction* i){cout << i->type << endl; return 0;});
			
			pmc_name.push_back("inst_count");
			pmc_vector.push_back([&](instruction* i){return 1;});
			
			pmc_name.push_back("load_count");
			pmc_vector.push_back([&](instruction* i){return (i->type == "load")?1:0;});
			
			pmc_name.push_back("store_count");
			pmc_vector.push_back([&](instruction* i){return (i->type == "store")?1:0;});
			
			pmc_name.push_back("alu_count");
			pmc_vector.push_back([&](instruction* i){return (i->type == "alu")?1:0;});
			
			pmc_name.push_back("control_count");
			pmc_vector.push_back([&](instruction* i){return (i->type == "control")?1:0;});
			
			//TODO I need counters which depend on dependencies
			//TODO define more counter functions

			// initialze counts
			for(unsigned int i=0; i<pmc_vector.size(); ++i){
				count_vector.push_back(0);
			}
		}

		void update(instruction* inst){
			// update counters based on instruction
			// runs the pmc function and increments count
			for(unsigned int i=0; i<pmc_vector.size(); ++i){
				count_vector[i] += pmc_vector[i](inst);
			}
		}

	private:
		vector<function<int(instruction* i)>> pmc_vector;
};

class processor{
	public:
		// every x instructions, read the pmu values
		const int MEASUREMENT_FREQUENCY = 1;

		// stores a record of all pmu counters
		vector<vector<int>> pmu_record;
		pmu* monitor;

		processor(string isa_file){
			monitor = new pmu();

			// processor should read its isa
			read_isa_file(isa_file);
		}

		void execute(instruction* i){	
			int inst_cycles = 1;

			// determine the number of cycles the instruction takes
			for(vector<instruction*>::iterator it = isa.begin(); it != isa.end(); ++it){
				if(i->type == (*it)->type){
					inst_cycles = (*it)->cycles;
					break;
				}
			}

			for(int j=0; j<inst_cycles; j++){
				++cycle;	
				if(cycle % MEASUREMENT_FREQUENCY == 0){
					pmu_record.push_back(monitor->count_vector);
				}
			}
		
			//TODO should this happen before or after instruction runs	
			// update after => update at retirement
			monitor->update(i);
		}


	private:
		int cycle;
		vector<instruction*> isa;
		
		void read_isa_file(string file_name){
			xml_document doc;

			doc.load_file(file_name.c_str());

			for(xml_node node = doc.first_child(); node; node = node.next_sibling()){
				instruction* inst = new instruction();
				inst->type = node.name();
				
				inst->cycles = atoi(node.attribute("cycles").value());
				//TODO read any additional information about the instriuction

				isa.push_back(inst);
			}
		}
};

//TODO
#include "cfg.cpp"

class program_fabricator{
	public:
		CFG* cfg;	

		program_fabricator(vector<vector<int>> record, vector<string> pmc_name){
			// rebuild the program given the pmu record
			cfg = new CFG();

			// update cfg based on record
			vector<vector<int>>::iterator p_it = record.begin();
			for(vector<vector<int>>::iterator it = record.begin()+1; 
					it != record.end() && p_it != record.end(); ++it){
				// create pmu delta values
				vector<int> delta;

				for(unsigned int i=0; i<p_it->size(); ++i){
					delta.push_back((*it)[i] - (*p_it)[i]);
				}

				// create new set based on delta values
				cfg->set_update(delta, pmc_name);

				++p_it;
			}
		}

		void print(){
			// print the refabricated program
			cfg->print();
		}

	private:
};


void read_inst_file(string file_name, queue<instruction*>& iq){
	xml_document doc;

	doc.load_file(file_name.c_str());
	
	for(xml_node node = doc.first_child(); node; node = node.next_sibling()){
		instruction* inst = new instruction();
		inst->type = node.name();

		// read dependence information
		inst->src0 = node.attribute("src0").value();
		inst->src1 = node.attribute("src1").value();
		inst->dest = node.attribute("dest").value();
		//TODO read any additional values
		
		iq.push(inst);
	}
}

int main(){
	string PROGRAM_FILE = "src/program.xml";
	string ISA_FILE = "src/isa.xml";

	queue<instruction*> instruction_queue;
	processor* p = new processor(ISA_FILE);

	// populate instruction queue
	read_inst_file(PROGRAM_FILE, instruction_queue);

	// execute all instructions
	while(!instruction_queue.empty()){
		p->execute(instruction_queue.front());
		instruction_queue.pop();
	}

	// try to piece back together the program with the pmu_record
	program_fabricator* pf = new program_fabricator(p->pmu_record, p->monitor->pmc_name);
	pf->print();

	return 0;
}
