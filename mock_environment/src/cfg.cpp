#include <vector>
#include <string>
#include <iostream>

using namespace std;

class instruction;

class node{
	public:
		instruction* inst;

		node(){}
	private:
};

class edge{
	public:
		node* src, dest;

		edge(){}

	private:
};

/**
 * instructions are nodes
 * edges contain a probability that the edge is taken
 *
 * Rules are applied to CFG to update edges
 */
class CFG{
	public:
		vector<node*> node_vector;
		vector<edge*> edge_vector;

		// indicates that nodes are part of a measurement set
		vector<vector<node*>> set_vector_vector;
		
		CFG(){}

		// create a new set based on pmu delta values
		void set_update(vector<int> pmu_delta, vector<string> pmc_name){
			vector<int>::iterator delta_it = pmu_delta.begin();
			vector<string>::iterator name_it = pmc_name.begin();
			while(delta_it != pmu_delta.end() && name_it != pmc_name.end()){
				// do something for each counter
				//TODO

				++delta_it;
				++name_it;
			}

			//TODO
		}

		void print(){
			//TODO
		}

	private:
};
